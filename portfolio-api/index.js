const express = require('express');
const bodyParser = require('body-parser')
const PORT = 3000;
const crypto = require('crypto');

const app = express();

app.use(bodyParser.json());

let videos = [
    {   
        id: 1,
        title: 'Mario Super Show Theme',
        subtitle: 'Do the Mario',
        img: 'https://www.retrones.net/sites/default/files/fotos-series/thCA8G79II.jpg'
    },
    {
        id: 2,
        title: 'Sonic Says',
        subtitle: "It's no good",
        img: 'https://picon.ngfiles.com/856000/flash_856590_card.png?f1662914633'
    },
    {
        id: 3,
        title: 'The Legend of Zelda Cartoon',
        subtitle: "Excuuuuse me princess",
        img: 'https://cdn.vox-cdn.com/thumbor/zZ4NfGpEVRFvCbqWtuSdAfhTaCU=/1400x0/filters:no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/24338042/Screenshot__170_.png'
    }
]

let coments = [
    {
        id: 1,
        id_video: 1,
        author: 'Fake',
        text: 'vaya mierdaca de vídeo'
    }
]

// Para evitar el error de CORS
/* app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE','PATCH','UPDATE');
    next();
}); */
const cors=require('cors');
app.use(cors())

app.listen(
    PORT,
    () => console.log('está vivo en http://localhost:'+ PORT)
)

// --- LOGIN ------------------------------------------------------------------ //

app.post('/login', (req, res) => {
    console.log(req.body);
    if (req.body.email === 'gismou@gmail.com' && req.body.password === '123456') {
        let token = crypto.randomBytes(64).toString('hex');
        return res.status(200).send({login_correct: true, token_access: token});
    } else {
        return res.status(200).send({login_correct: false});
    }
})

// --- VIDEOS ----------------------------------------------------------------- //

app.get('/videos', (req, res) => {
    res.status(200).send(videos);
})

app.get('/videos/:id', (req, res) => {
    let video = videos.find(c => c.id === parseInt(req.params.id));
    if(!video) {
        return res.status(400).send('Vídeo no encontrado');
    } else {
        return res.status(200).send(video);
    };
})

// --- COMENTS -----------------------------------------------------------------//

app.post('/coments/:id',(req, res) => {
    let lastCommentId = coments[coments.length-1].id;
    coments.push({id:lastCommentId+1, id_video:parseInt(req.params.id), author:'Fake', text:req.body.mensaje})
    return res.status(200).send(coments);
})

app.get('/coments',(req,res) => {
    return res.status(200).send(coments);
})

app.get('/coments/:id',(req, res) => {
    let video = videos.find(c => c.id === parseInt(req.params.id));
    if(!video) {
        return res.status(400).send('Vídeo no encontrado')
    }else {
        let comentsOfVideo = coments.filter(coment => coment.id_video === parseInt(req.params.id));
        return res.status(200).send(comentsOfVideo);
    };
})