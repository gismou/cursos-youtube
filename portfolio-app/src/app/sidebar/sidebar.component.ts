import { Component, OnInit } from '@angular/core';
import { ServicioDeFavoritosService } from '../services/servicioDeFavoritos/servicio-de-favoritos.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public listaDeVideos: Array<any> = [];

  constructor(private servicioDeFavoritosService: ServicioDeFavoritosService){};

  ngOnInit(): void {
    this.servicioDeFavoritosService.disparadorDeFavoritos.subscribe(data => {
      this.listaDeVideos.push(data);
    })
  }

}
