import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestService } from '../services/rest/rest.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-video-detail',
  templateUrl: './video-detail.component.html',
  styleUrls: ['./video-detail.component.css']
})
export class VideoDetailComponent implements OnInit {

  id_video: any;
  respuesta: any = [];
  comentarios: any = [];
  comentarioTexto: string;
  form: FormGroup;

  constructor(private route: ActivatedRoute, private restService: RestService, private formBuilder: FormBuilder){ }

  ngOnInit(): void {

    this.form = this.formBuilder.group({
      textAreaComentario: ['']
    })

    this.route.paramMap.subscribe( (paramMap:any) => {
      const {params} = paramMap;
      this.cargarData(parseInt(params.id));
      this.cargarComentarios(parseInt(params.id));
      this.id_video = parseInt(params.id);
    })
    console.log(this.comentarios);

  }

  cargarData(id:any){
    this.restService.get("http://localhost:3000/videos/"+id)
      .subscribe(respuesta => {
        this.respuesta = respuesta;
      })
  }

  cargarComentarios(id_video:any){
    this.restService.get('http://localhost:3000/coments/'+id_video)
    .subscribe(respuesta => {
      this.comentarios = respuesta;
    })
  }

  enviarComentario(id_video:any,formulario:any,form:{mensaje: string}){
    this.restService.post('http://localhost:3000/coments/'+id_video,form)
    .subscribe(respuesta => {
      this.cargarComentarios(id_video);
      formulario.resetForm();
      console.log(respuesta);
    })
  }

}
