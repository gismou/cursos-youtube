import { Component } from '@angular/core';
import { RestService } from '../services/rest/rest.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  public listaVideos: any = [];

  constructor(private restService:RestService){}

  ngOnInit(): void {
    this.cargarData();
  }

  public cargarData(){
    this.restService.get('http://localhost:3000/videos')
    .subscribe(respuesta => {
      console.log(respuesta);
      this.listaVideos = respuesta;
    })
  }

}
