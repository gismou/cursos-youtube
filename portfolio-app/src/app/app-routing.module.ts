import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VideoDetailComponent } from './video-detail/video-detail.component';
import { HomeComponent } from './home/home.component';
import { ListViewsComponent } from './list-views/list-views.component';
import { LoginComponent } from './login/login.component';
import { vigilanteLoginGuard } from './vigilante-login.guard';

const routes: Routes = [
  {
    path:'videos/:id',
    component:VideoDetailComponent,
    canActivate: [vigilanteLoginGuard]
  },
  {
    path:'',
    component:HomeComponent,
    canActivate: [vigilanteLoginGuard]
  },
  {
    path:'list-videos',
    component:ListViewsComponent,
    canActivate: [vigilanteLoginGuard]
  },
  {
    path:'login',
    component:LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
