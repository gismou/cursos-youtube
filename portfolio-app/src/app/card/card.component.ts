import { Component, Input, OnInit } from '@angular/core';
import { ServicioDeFavoritosService } from '../services/servicioDeFavoritos/servicio-de-favoritos.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit{
  @Input() dataEntrante: any;
  public image: string = "";

  constructor(private servicioDeFavoritosService: ServicioDeFavoritosService){}

  ngOnInit(): void{
    this.image = 'https://picsum.photos/536/354'
  }

  agregarFavoritos(){
    this.servicioDeFavoritosService.disparadorDeFavoritos.emit({
      data:this.dataEntrante
    })
  }
}
