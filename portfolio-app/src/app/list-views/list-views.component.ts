import { RestService } from './../services/rest/rest.service';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { ColumnMode } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-list-views',
  templateUrl: './list-views.component.html',
  styleUrls: ['./list-views.component.css']
})
export class ListViewsComponent {
  @ViewChild('editTmpl', { static: true }) editTmpl: TemplateRef<any>;
  @ViewChild('hdrTpl', { static: true }) hdrTpl: TemplateRef<any>;

  data : any[] = [];
  cols : any[] = [];

  ColumnMode = ColumnMode;

  constructor(private RestService:RestService) {

  }

  ngOnInit() {
    this.cols = [
      {
        cellTemplate: this.editTmpl,
        headerTemplate: this.hdrTpl,
        name: 'title'
      },
      {
        cellTemplate: this.editTmpl,
        headerTemplate: this.hdrTpl,
        name: 'subtitle'
      },
      {
        cellTemplate: this.editTmpl,
        headerTemplate: this.hdrTpl,
        name: 'img'
      }
    ];
    this.cargarData()
  }

  cargarData(): void {
    this.RestService.get('http://localhost:3000/videos')
    .subscribe((data:any) => [
      this.data = data
    ])
  }
}
