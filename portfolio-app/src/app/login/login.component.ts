import { RestService } from './../services/rest/rest.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit{

  public formLogin: FormGroup;

  constructor(private formBuilder: FormBuilder, private restService: RestService,
              private cookieService: CookieService, private router: Router){
  }

  ngOnInit(): void {
    // borrar cookie del token
      this.cookieService.delete('token_access')
    // crear formBuilder
    this.formLogin = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      terms: ['', [Validators.required, Validators.requiredTrue]]
    })
  }

  send(): any {
    this.restService.post('http://localhost:3000/login', this.formLogin.value)
    .subscribe((res: any) => {
      if(res.login_correct){
        console.log(res);
        this.cookieService.set('token_access', res.token_access, 4, '/');
        this.router.navigate(['/']);
      }else{
        console.log(res);
        alert('Datos de acceso incorrectos');
      }
    })
  }

}
