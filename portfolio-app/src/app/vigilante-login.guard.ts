import { Injectable, inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';


@Injectable()
 export class GuardService {

  constructor(private cookieService: CookieService,public router: Router) { }

  canActivate(): boolean {
    const cookie = this.cookieService.check('token_access');
    if (cookie) {
      return true
    } else {
      this.router.navigate(['/login']);
      return false
    }
   }

 }

export const vigilanteLoginGuard: CanActivateFn = (route, state) => {
  console.log(inject(GuardService).canActivate());
  return inject(GuardService).canActivate();
};
