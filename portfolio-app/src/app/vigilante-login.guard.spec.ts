import { TestBed } from '@angular/core/testing';
import { CanActivateFn } from '@angular/router';

import { vigilanteLoginGuard } from './vigilante-login.guard';

describe('vigilanteLoginGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => vigilanteLoginGuard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});
