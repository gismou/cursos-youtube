export class Video{
  constructor(
    public id:number,
    public title:string,
    public subtitle:string,
    public img:string
  ){}
}
